const { CreateTableCommand,
  ListTablesCommand,
  GetItemCommand,
  PutItemCommand,
  ScanCommand
} = require("@aws-sdk/client-dynamodb")
// Valores usados para manejar estado de la app
let caracPrevios = {}
let cuentas = {}
let exito = false
let dbInit = false
let recienCreada = false
let registroEncontrado = false

/*
  Definición de método para hash
  https://stackoverflow.com/questions/7616461/generate-a-hash-from-string-in-javascript
*/
String.prototype.hashCode = function() {
  var hash = 0, i, chr
  if (this.length === 0) return hash
  for (i = 0; i < this.length; i++) {
    chr   = this.charCodeAt(i)
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0 // Convert to 32bit integer
  }
  return hash
}

// Generación de Hash por cadena de DNA
function generaHash (cadenaDNA) {
  let cadena = cadenaDNA.join('')
  return cadena.hashCode()
}

// Inicialización de valores
function iniciaValores() {
  caracPrevios = {
    vertical: '',
    horizontal: '',
    oblicuo1: '',
    oblicuo2: '',
    oblicuo3: '',
    oblicuo4: '',
    oblicuo5: '',
    oblicuo6: '',
    oblicuo7: '',
    oblicuo8: '',
    oblicuo9: '',
    oblicuo10: ''
  }
  cuentas = {
    vertical: 0,
    horizontal: 0,
    oblicuo1: 0,
    oblicuo2: 0,
    oblicuo3: 0,
    oblicuo4: 0,
    oblicuo5: 0,
    oblicuo6: 0,
    oblicuo7: 0,
    oblicuo8: 0,
    oblicuo9: 0,
    oblicuo10: 0
  }
  exito = false
}

// Verificación de la BD (Un poco redundante pero es mejor prevenir)
async function verificaDB (clienteDB) {
  if (!dbInit) {
    try {
      const data = await clienteDB.send(new ListTablesCommand({}))
      // Sólo tenemos una Tabla, basta con ver si hay tablas o no
      if (data.TableNames.length === 0) {
        const params = {
          AttributeDefinitions: [
            {
              AttributeName: "HashCode",
              AttributeType: "N",
            }
          ],
          KeySchema: [
            {
              AttributeName: "HashCode",
              KeyType: "HASH",
            }
          ],
          ProvisionedThroughput: {
            ReadCapacityUnits: 1,
            WriteCapacityUnits: 1,
          },
          TableName: "registroDNA",
          StreamSpecification: {
            StreamEnabled: false,
          }
        }
        await clienteDB.send(new CreateTableCommand(params))
        recienCreada = true
      }
      dbInit = true
    } catch (error) {
      throw error
    }
  }
}

// Validación de un registro en específico
async function existeRegistro(codigoHash, clienteDB) {
  await verificaDB(clienteDB)
  if (recienCreada) {
    recienCreada = false
    return false
  } else {
    const params = {
      TableName: "registroDNA",
      Key: {
        HashCode: { N: codigoHash.toString() },
      },
      ProjectionExpression: "Veredicto"
    }
    const data = await clienteDB.send(new GetItemCommand(params));
    if ((data.Item === undefined) ||
      (!Object.prototype.hasOwnProperty.call(data.Item, 'Veredicto'))) {
      exito = false
      return false
    } else {
      exito = data.Item.Veredicto.BOOL
      return true
    }
  }
}

// Generación de un registro en específico
async function creaRegistro (codigoHash, clienteDB) {
  await verificaDB(clienteDB)
  const params = {
    TableName: "registroDNA",
    Item: {
      HashCode: { N: codigoHash.toString() },
      Veredicto: { BOOL: exito }
    }
  }
  const data = await clienteDB.send(new PutItemCommand(params));
}

// Función principal
async function hasMutation (dna, clienteDB) {
  iniciaValores()
  let concentrado = dna
  const codigoHash = generaHash (concentrado)
  // Verificamos si no existe previamente el registro
  let resp;
  if (process.env.NODE_ENV !== 'test') {
    resp = await existeRegistro(codigoHash, clienteDB)
  } else {
    resp = false
  }
  if (!resp) {
    loop1:
      for (let i = 0; i < 6; i++) {
    loop2:
        for (let j = 0; j < 6; j++) {
          if (comparaPrevio(concentrado[i][j], 'horizontal')) {
            break loop1
          }
          if (comparaPrevio(concentrado[j][i], 'vertical')) {
            break loop1
          }
          if ((i === 3 && j === 0) || (i === 2 && j === 1) || (i === 1 && j === 2) || (i === 0 && j === 3)) {
            if (comparaPrevio(concentrado[j][i], 'oblicuo1')) {
              break loop1
            }
          }
          if ((i === 4 && j === 0) || (i === 3 && j === 1) || (i === 2 && j === 2) || (i === 1 && j === 3) || (i === 0 && j === 4)) {
            if (comparaPrevio(concentrado[j][i], 'oblicuo2')) {
              break loop1
            }
          }
          if ((i === 5 && j === 0) || (i === 4 && j === 1) || (i === 3 && j === 2) || (i === 2 && j === 3) || (i === 1 && j === 4) || (i === 0 && j === 5)) {
            if (comparaPrevio(concentrado[j][i], 'oblicuo3')) {
              break loop1
            }
          }
          if ((i === 5 && j === 1) || (i === 4 && j === 2) || (i === 3 && j === 3) || (i === 2 && j === 4) || (i === 1 && j === 5)) {
            if (comparaPrevio(concentrado[j][i], 'oblicuo4')) {
              break loop1
            }
          }
          if ((i === 5 && j === 2) || (i === 4 && j === 3) || (i === 3 && j === 4) || (i === 2 && j === 5)) {
            if (comparaPrevio(concentrado[j][i], 'oblicuo5')) {
              break loop1
            }
          }
          if ((i === 0 && j === 0) || (i === 1 && j === 1) || (i === 2 && j === 2) || (i === 3 && j === 3) || (i === 4 && j === 4) || (i === 5 && j === 5)) {
            if (comparaPrevio(concentrado[j][i], 'oblicuo6')) {
              break loop1
            }
          }
          if ((i === 0 && j === 1) || (i === 1 && j === 2) || (i === 2 && j === 3) || (i === 3 && j === 4) || (i === 4 && j === 5)) {
            if (comparaPrevio(concentrado[j][i], 'oblicuo7')) {
              break loop1
            }
          }
          if ((i === 1 && j === 0) || (i === 2 && j === 1) || (i === 3 && j === 2) || (i === 4 && j === 3) || (i === 5 && j === 4)) {
            if (comparaPrevio(concentrado[j][i], 'oblicuo8')) {
              break loop1
            }
          }
          if ((i === 2 && j === 0) || (i === 3 && j === 1) || (i === 4 && j === 2) || (i === 5 && j === 3)) {
            if (comparaPrevio(concentrado[j][i], 'oblicuo9')) {
              break loop1
            }
          }
          if ((i === 0 && j === 2) || (i === 1 && j === 3) || (i === 2 && j === 4) || (i === 3 && j === 5)) {
            if (comparaPrevio(concentrado[j][i], 'oblicuo10')) {
              break loop1
            }
          }
        }
        cuentas['horizontal'] = 0
        cuentas['vertical'] = 0
        caracPrevios['horizontal'] = ''
        caracPrevios['vertical'] = ''
      }
      if (process.env.NODE_ENV !== 'test') {
        await creaRegistro(codigoHash, clienteDB)
      }
  }
  return exito
}

// Función central de comparación
function comparaPrevio (carActual, tipo) {
  if (caracPrevios[tipo] !== carActual) {
    cuentas[tipo] = 0
  } else {
    if (cuentas[tipo] === 2) {
      // console.log('->', tipo) // Quitar comentario para ver tipo de anomalía
      exito = true
    } else {
      cuentas[tipo]++
    }
  }
  caracPrevios[tipo] = carActual
  return exito
}

// Validación de datos de entrada
function validaDatos (body) {
  if (!Object.prototype.hasOwnProperty.call(body, 'dna') && !Object.prototype.hasOwnProperty.call(body, 'DNA')) {
    throw new Error("El cuerpo de la petición no contiene algún elemento llamado 'dna'")
  }
  if (!Array.isArray(body.dna)) {
    throw new Error("El campo 'dna' de la petición no es un arreglo")
  }
  if (body.dna.length !== 6) {
    throw new Error("El campo 'dna' no tiene la longitud correcta")
  }
  for (let i = 0; i < body.dna.length; i++) {
    if (typeof (body.dna[i]) !== 'string') {
      throw new Error('El elemento ' + (i + 1) + ' de DNA no es un una cadena de caracteres')
    }
    if (body.dna[i].length !== 6) {
      throw new Error('La cadena ' + (i + 1) + ' no cuenta con la longitud debida')
    }
  }
  return true
}

// Función para la obtención de estadísticas
async function devuelveStats (clienteDB) {
  await verificaDB(clienteDB)
  // Consultamos por los positivos
  let params = {
    FilterExpression: 'Veredicto = :b',
    ExpressionAttributeValues: {
         ':b': { BOOL: true }
    },
    ProjectionExpression: 'Veredicto',
    TableName: 'registroDNA'
  }
  const resPosi = await clienteDB.send(new ScanCommand(params))
  const positivos = resPosi.Count

  // Consultamos por los negativos
  params['ExpressionAttributeValues'] = {
         ':b': { BOOL: false }
  }
  const resNega = await clienteDB.send(new ScanCommand(params))
  const negativos = resNega.Count

  let ratio = 0
  if (negativos == 0) {
    ratio = positivos
  } else {
    ratio = (positivos * 1.0) /negativos
  }
  return {
    count_mutations: positivos,
    count_no_mutation: negativos,
    ratio: ratio
  }
}

exports.hasMutation = hasMutation
exports.devuelveStats = devuelveStats
exports.validaDatos = validaDatos
exports.verificaDB = verificaDB
