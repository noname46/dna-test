const logica = require('../logica')

describe("Función de validación", () => {
  test("Debe detectar cuando se envían cadenas de menos o más", async () => {
    const datos = {
      dna: ["ATGCGA","CAGTGC","TTATGT","AGACGG","CCCCTA"]
    }
    try {
      logica.validaDatos(datos)
    } catch (e) {
      expect(e.message).toBe("El campo 'dna' no tiene la longitud correcta");
    }
  });

  test("Debe detectar cuando no se envían las cadenas de adn", async () => {
    const datos = {}
    try {
      logica.validaDatos(datos)
    } catch (e) {
      expect(e.message).toBe("El cuerpo de la petición no contiene algún elemento llamado 'dna'");
    }
  });

  test("Debe detectar cuando se envía algo distinto a un arreglo", async () => {
    const datos = {
      dna: {
        1: "ATGCGA",
        2: "CAGTGC",
        3: "TTATGT",
        4: "AGACGG",
        5: "CCCCTA",
        6: "CATCCA"
      }
    }
    try {
      logica.validaDatos(datos)
    } catch (e) {
      expect(e.message).toBe("El campo 'dna' de la petición no es un arreglo");
    }
  });

  test("Debe detectar cuando alguna cadena tenga un valor incorrecto", async () => {
    const datos = {
      dna: ["ATGCGAC","CAGTGC","TTATGT","AGACGG","CCCCTA", "CATCCA"]
    }
    try {
      logica.validaDatos(datos)
    } catch (e) {
      expect(e.message).toBe("La cadena 1 no cuenta con la longitud debida");
    }
  });
});
