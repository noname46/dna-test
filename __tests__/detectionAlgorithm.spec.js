const logica = require('../logica')

describe("Función de detección", () => {
  test("Debe detectar anomalías horizontales", async () => {
    const dna = ["ATGCGA","CAGTGC","TTATGT","AGACGG","CCCCTA","TCACTG"]
    const res = await logica.hasMutation(dna, null)
    expect(res).toEqual(true);
  });
  test("Debe detectar anomalías verticales", async () => {
    const dna = ["ATGCGA","CAGTGC","TTCTGG","AGAAGG","CGCCTG","TCACTG"]
    const res = await logica.hasMutation(dna, null)
    expect(res).toEqual(true);
  });
  test("Debe detectar anomalías oblícuas en dirección 45°", async () => {
    const dna = ["GAAATA","AGAACG","AACAAT","CAAGAA","AGATAC","CAACCA"]
    const res = await logica.hasMutation(dna, null)
    expect(res).toEqual(true);
  });
  test("Debe detectar anomalías oblícuas en dirección 135°", async () => {
    const dna = ["GACATA","AGCTCG","ATGCCT","TCCGTT","ACAAGC","CATCCA"]
    const res = await logica.hasMutation(dna, null)
    expect(res).toEqual(true);
  });
  test("Debe responder negativo cuando no hay anomalías presentes", async () => {
    const dna = ["GACTTA","AGTCCG","ACCACT","TAAGTT","AGAAGC","CATCCA"]
    const res = await logica.hasMutation(dna, null)
    expect(res).toEqual(false);
  });
});
