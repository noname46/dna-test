# DNA-Tester

Esta app determina si una persona presenta diferencias genéticas basándose en su secuencia de ADN.
Dicha app ya se encuentra alojada en AWS en las siguientes URL:

POST - https://h1jl4jw951.execute-api.us-west-2.amazonaws.com/prod/mutation \
En esta URL se encuentra el método de análisis de muestras de ADN el cual admite peticiones del tipo JSON con un elemento Array de Strings llamado 'adn'.

GET - https://h1jl4jw951.execute-api.us-west-2.amazonaws.com/prod/stats \
En esta otra URL se encuentra el método de visualización de la estadísticas de los registros de ADN la BD.

## Instalación
Para instalar esta app basta con ejecutar los siguientes comandos. Esto, asumiendo que [nodejs](https://nodejs.org/es/) se encuentra instalado en su computadora.
```bash
npm run install
sls dynamodb install
```
Dichos comandos instalarán las dependencias de Nodejs y descargarán la versión local de la base de datos DynamoDB. Es probable que al ejecutar el segundo comando necesite proveer credenciales de aws sdk.

## Ejecución (local)
Para que la app se ejecute de forma local es necesario ejecutar el siguiente comando una vez que esta ha sido instalada.
```bash
sls offline start
```
Una vez que se haya iniciado el sistema, las siguientes URLs se encontrarán habilitadas:\
POST - http://localhost:3000/dev/mutation \
GET - http://localhost:3000/dev/stats

## Pruebas
Para ejecutar las pruebas al algoritmo de detección y a la función de verificación de datos solamente es necesario ejecutar el siguiente comando.
```bash
npm run test
```


## Eliminación
Para eliminar la app de sus sistema es necesrio ejecutar el siguiente comando.
```bash
sls dynamodb remove
```
Una vez que se ha eliminado la versión local de DynamoDB sólo es necesario eliminar el directorio y sus contenidos.
