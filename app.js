'use strict'
require('dotenv').config()
const { DynamoDBClient } = require("@aws-sdk/client-dynamodb")
const express = require('express')
const logica = require('./logica')
const sls = require('serverless-http')

// Configuración
const app = express()
app.use(express.json())

let clienteDB;
if (process.env.NODE_ENV === 'dev') {
  clienteDB = new DynamoDBClient({
      region: 'localhost',
      endpoint: 'http://localhost:8000'
  })
} else {
  clienteDB = new DynamoDBClient({
    region: "us-west-2"
  })
}

/*
Ruta de petición para validación
*/
app.post('/mutation', async (req, res) => {
  try {
    if (logica.validaDatos(req.body)) {
      const resp = await logica.hasMutation(req.body.dna, clienteDB)
      if (resp) {
        res.status(200).send()
      } else {
        res.status(403).send()
      }
    }
  } catch (error) {
    console.log('ERROR: ', error)
    res.status(500).send(error.message)
  }
})

/*
Ruta de petición para estadísticas
*/
app.get('/stats', async (req, res) => {
  try {
    const resp = await logica.devuelveStats(clienteDB)
    res.status(200).send(resp)
  } catch (error) {
    res.status(500).send(error.message)
  }
})

// Verificamos que la tabla exista antes de escuchar por peticiones
module.exports.handler = sls(app)
